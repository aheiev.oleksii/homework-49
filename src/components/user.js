export class User {
    constructor(args) {
        this.id = args.id;
        this.name = args.name;
        this.age = args.age;
        this.email = args.email;
        this.country = args.country;
        this.company = args.company;
        this.birthday = args.birthday;
        this.gender = args.gender;
    }

    render() {
        const usersList = document.querySelector('#usersList');
        const li = document.createElement('li');
        const userId = document.createElement('p');
        const userName = document.createElement('p');
        const userAge = document.createElement('p');
        const userEmail = document.createElement('p');
        const userCountry = document.createElement('p');
        const userCompany = document.createElement('p');
        const userBirthday = document.createElement('p');
        const userGender = document.createElement('p');

        userId.innerText = `User id: ${this.id}`;
        userName.innerText = `User name: ${this.name}`;
        userAge.innerText = `User age: ${this.age}`;
        userEmail.innerText = `User Email: ${this.email}`;
        userCountry.innerText = `Country: ${this.country}`;
        userCompany.innerText = `Company: ${this.company}`;
        userBirthday.innerText = `Birthday: ${this.birthday}`;
        userGender.innerText = `Gender: ${this.gender}`;

        li.append(userName, userAge, userBirthday, userGender, userEmail, userId, userCountry, userCompany);

        if (this.gender === 'male') {
            const maleUsersList = document.querySelector('#maleUsersList');
            maleUsersList.append(li);
        } else if (this.gender === 'female') {
            const femaleUsersList = document.querySelector('#femaleUsersList');
            femaleUsersList.append(li);
        } else if (this.gender === 'boy' || this.gender === 'girl') {
            const childrenList = document.querySelector('#childrenList');
            childrenList.append(li);
        } else {
            usersList.append(li);
        }
    }
}
