import { Children } from './children';
import { Male } from './male';
import { Female } from './female';

export function filterUsers(users) {
    users.filter((user) => {
        if (user.gender === 'girl' || user.gender === 'boy') {
            const child = new Children(user);
            child.render();
        } else if (user.gender === 'male') {
            const male = new Male(user);
            male.render();
        } else {
            const female = new Female(user);
            female.render();
        }
    });
}
